<?php

namespace Drupal\svg_embed;

/**
 * Interface for the Processor.
 *
 * @package Drupal\svg_embed
 */
interface SvgEmbedProcessInterface {

  /**
   * A method to translate SVG elements.
   *
   * @param string $uuid
   *   The file object UUId.
   * @param string $langcode
   *   The file's language.
   *
   * @return string
   *   The text translation.
   */
  public function translate(string $uuid, string $langcode): string;

}
