<?php

namespace Drupal\svg_embed;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use enshrined\svgSanitize\Sanitizer;

/**
 * Processor for SVGs.
 *
 * @package Drupal\svg_embed
 */
class SvgEmbedProcess implements SvgEmbedProcessInterface {

  /**
   * The entity manager object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * A database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $connection;

  /**
   * SvgEmbedProcess constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager object.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler object.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler, Connection $connection) {
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function translate(string $uuid, string $langcode): string {
    $xml = $this->loadFile($uuid);

    if ($this->moduleHandler->moduleExists('locale')) {
      // Go through the DOM and translate all relevant strings.
      $this->embedTranslate($xml, $langcode);
    }

    // Strip early comments and a potential xml tag.
    $svg = $xml->asXML();
    $svg_tag = strpos($svg, '<svg');
    return substr($svg, $svg_tag);
  }

  /**
   * Load an SVG file.
   *
   * @param string $uuid
   *   The file's UUID.
   *
   * @return \SimpleXMLElement
   *   The file as an XML object.
   *
   * @throws \Exception
   */
  private function loadFile(string $uuid): \SimpleXMLElement {
    $text = '';
    try {
      /** @var \Drupal\file\Entity\File[] $files */
      $files = $this->entityTypeManager->getStorage('file')->loadByProperties(['uuid' => $uuid]);
      if ($files) {
        $file = reset($files);
        $text = file_get_contents($file->getFileUri());

        // Sanitize the original SVG file content.
        $sanitizer = new Sanitizer();
        $text = $sanitizer->sanitize($text);
      }
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException) {
      // @todo log this exception.
    }
    return new \SimpleXMLElement($text);
  }

  /**
   * Helper function called recursively to translate all strings in an SVG file.
   *
   * @param \SimpleXMLElement $xml
   *   The SVG graphic code.
   * @param string $langcode
   *   The language code to which we need to translate.
   */
  protected function embedTranslate(\SimpleXMLElement $xml, string $langcode): void {
    foreach ($xml as $child) {
      $this->embedTranslate($child, $langcode);
      if (isset($child->text) || isset($child->tspan)) {
        if (isset($child->text->tspan)) {
          $text = $child->text->tspan;
        }
        elseif (isset($child->tspan)) {
          $text = $child->tspan;
        }
        else {
          $text = $child->text;
        }
        $i = 0;
        while (TRUE) {
          $string = (string) $text[$i];
          if (empty($string)) {
            break;
          }
          $string = trim($string);
          if (!empty($string)) {
            $query = $this->connection->select('locales_source', 's');
            $query->leftJoin('locales_target', 't', 's.lid = t.lid');
            $translation = $query->fields('t', ['translation'])
              ->condition('s.source', $string)
              ->condition('s.context', 'svg_embed')
              ->condition('t.language', $langcode)
              ->execute()
              ->fetchField();
            $text[$i][0] = empty($translation) ? $string : $translation;
          }
          $i++;
        }
      }
    }
  }

}
