<?php

namespace Drupal\svg_embed\Plugin\Filter;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\file\Entity\File;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\svg_embed\SvgEmbedProcessInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a filter to embed and translate SVG images.
 *
 * @Filter(
 *   id = "svg_embed",
 *   title = @Translation("Embed and translate SVG images"),
 *   description = @Translation("Allows to embed SVG graphics into text like
 *   with images and even translates text strings in the SVG file to the
 *   language of the node."), type =
 *   Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE
 * )
 */
class SvgEmbed extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * An entity manager object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The processor class for SVGs.
   *
   * @var \Drupal\svg_embed\SvgEmbedProcessInterface
   */
  protected SvgEmbedProcessInterface $svgEmbedProcess;

  /**
   * Constructs a \Drupal\svg_embed\Plugin\Filter\SvgEmbed object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\svg_embed\SvgEmbedProcessInterface $svg_embed_process
   *   The processor class from this module.
   */
  final public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, SvgEmbedProcessInterface $svg_embed_process) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->svgEmbedProcess = $svg_embed_process;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): SvgEmbed {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('svg_embed.process')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @throws \DOMException
   */
  public function process($text, $langcode): FilterProcessResult {
    $result = new FilterProcessResult($text);
    $processed_uuids = [];

    // Do we have at least one SVG reference in the $text?
    if (stripos($text, 'data-entity-type="file"') !== FALSE || stripos($text, 'data-entity-type="media"') !== FALSE) {
      $dom = Html::load($text);
      $xpath = new \DOMXPath($dom);

      // Identify the SVG nodes from file embeds.
      /** @var \DOMElement $node */
      foreach ($xpath->query('//*[@data-entity-type="file" and @data-entity-uuid]') as $node) {
        if (method_exists($node, 'getAttribute')) {
          if ($node->tagName !== 'svg') {
            continue;
          }
          $uuid = $node->getAttribute('data-entity-uuid');
          // Only process the first occurrence of each file UUID.
          if (!isset($processed_uuids[$uuid])) {
            $processed_uuids[$uuid] = $this->svgEmbedProcess->translate($uuid, $langcode);
          }
          $node->parentNode->replaceChild($dom->createElement('svgembed', $uuid), $node);
        }
      }

      // Identify the SVG nodes from media embeds.
      /** @var \DOMElement $node */
      foreach ($xpath->query('//*[@data-entity-type="media" and @data-entity-uuid]') as $node) {
        if (method_exists($node, 'getAttribute')) {
          if ($node->tagName !== 'drupal-media') {
            continue;
          }
          $mediaUuid = $node->getAttribute('data-entity-uuid');
          try {
            /** @var \Drupal\media\Entity\Media[] $medias */
            $medias = $this->entityTypeManager->getStorage('media')->loadByProperties(['uuid' => $mediaUuid]);
            if (!$medias) {
              continue;
            }
            $media = reset($medias);
            $sourceField = $media->getSource()->getConfiguration()['source_field'];
            $fileId = $media->get($sourceField)->getValue()[0]['target_id'];
            $file = File::load($fileId);
            if (!$file) {
              continue;
            }
            $mime = $file->get('filemime')->value;
            if (!str_contains($mime, 'svg')) {
              // This filter should only operate with svg media.
              continue;
            }
            $uuid = $file->get('uuid')->value;
          }
          catch (InvalidPluginDefinitionException | PluginNotFoundException) {
            // @todo log this exception.
            continue;
          }

          // Only process the first occurrence of each file UUID.
          if (!isset($processed_uuids[$uuid])) {
            $processed_uuids[$uuid] = $this->svgEmbedProcess->translate($uuid, $langcode);
          }
          $node->parentNode->replaceChild($dom->createElement('svgembed', $uuid), $node);
        }
      }
      if ($processed_uuids) {
        $text = Html::serialize($dom);
      }
    }

    // Process SVG tokens.
    while ($pos = mb_strpos($text, '[svg:')) {
      $endPos = mb_strpos($text, ']', $pos) + 1;
      $tag = mb_substr($text, $pos, $endPos - $pos);
      [, $id] = explode(':', mb_substr($tag, 1, -1), 2);
      $file = NULL;
      if (is_numeric($id)) {
        $file = File::load($id);
      }
      else {
        try {
          /** @var \Drupal\file\Entity\File[] $files */
          $files = $this->entityTypeManager->getStorage('file')->loadByProperties(['filename' => $id]);
          if ($files) {
            $file = reset($files);
          }
        }
        catch (InvalidPluginDefinitionException | PluginNotFoundException) {
          // @todo log this exception.
        }
      }
      if ($file !== NULL) {
        $uuid = $file->get('uuid')->value;
        // Only process the first occurrence of each file UUID.
        if (!isset($processed_uuids[$uuid])) {
          $processed_uuids[$uuid] = $this->svgEmbedProcess->translate($uuid, $langcode);
        }
      }
      else {
        $uuid = (string) $this->t('SVG not found');
      }

      $text = mb_substr($text, 0, $pos) . '<svgembed>' . $uuid . '</svgembed>' . mb_substr($text, $endPos);
    }

    // Replace processed uuids.
    if ($processed_uuids) {
      foreach ($processed_uuids as $uuid => $svg) {
        $text = str_replace('<svgembed>' . $uuid . '</svgembed>', $svg, $text);
      }
      $result->setProcessedText($text);
    }

    return $result;
  }

}
